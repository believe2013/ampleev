const mix = require('laravel-mix');
mix.webpackConfig({ devtool: "inline-source-map" });
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js').sourceMaps()
    .sass('resources/assets/sass/app.scss', 'public/css').browserSync({
    proxy: {
        target: '127.0.0.1:8080',
        reqHeaders: function () {
            return { host: 'localhost:3000' };
        }
    }
});
