<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    private $timestamps = false;

    private $fillable = ['price', 'amount', 'sales', 'article', 'product_id'];


    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
