<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Category extends Model
{
    use NodeTrait;

    private $timestamps = false;

    private $fillable = ['title', 'alias'];

    // RELATIONS

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

}
