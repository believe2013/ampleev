<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    private $timestamps = false;

    private $fillable = ['title', 'image', 'description', 'first_invoice', 'last_supplied', 'url', 'price', 'amount'];

    // RELATIONS

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }
}
