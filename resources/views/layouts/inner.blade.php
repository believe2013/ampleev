<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('partials.head')
    @yield('customCss')
</head>
<body>
    <div class="super_container">
        @include('partials.header', ['showHero' => false])
        @yield('content')
    </div>
    @include('partials.footer')
    @include('partials.scripts')
    @yield('customJs')
</body>
</html>
