
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

require('scrollmagic');
require('./../vendor/OwlCarousel2-2.2.1/owl.carousel.js');
require('slick-carousel');
require('./../vendor/easing/easing.js');
//require('./theme.js');

